(function(){
var jquery_version = '3.3.1';
var site_url = 'https://7a70cd070737.ngrok.io/';
var static_url = site_url + 'static/';
var min_width = 100;
var min_height = 100;

function bookmarklet(msg) {
//loading css styles
var css = jQuery('<link>');
css.attr({
rel: 'stylesheet',
type: 'text/css',
href: static_url + 'account/css/bookmarklet.css?r=' + Math.floor(Math.random()*99999999999999999999)
});
jQuery('head').append(css);
//loading html code
box_html = '<div id="bookmarklet"><a href="#" id="close">&times;</a><h1>Pick image to add:</h1><div class="images"></div></div>';
jQuery('body').append(box_html);
//Close event
jQuery('#bookmarklet #close').click(function(){
jQuery('#bookmarklet').remove();
});

//check if the jquery library has been loaded
if(typeof window.jQuery != 'undefined') {
  bookmarklet();
} else {
//check conflicts
var conflict = typeof window.$ !='undefined';
//create script and API indication
var script = document.createElement('script');
script.setAttribute('src', 'http://ajax.googleapis.com/ajax/libs/jquery/' + jquery_version + '/jquery.min.js');
//add a tag script to a <head> script for processing
document.getElementsByTagName('head')[0].appendChild(script);
//mechanism that allows you to wait until the script is finished
var attempts = 15;
(function(){
//we check again whether we have defined jQuery
if(typeof window.jQuery == 'undefined') {
    if(--attempts > 0) {
    //calling the script within miliseconds
    window.setTimeout(arguments.callee, 250)
    } else {
    //too many try to attempts, error
    alert('Error')
    }
    } else {
        bookmarklet();
    }
})()
}
jQuery.each(jQuery('img[src$="jpg"]'), function(index, image) {
if(jQuery(image).width() >= min_width && jQuery(image).height() >= min_height)
{
image_url = jQuery(image).attr('src');
jQuery('#bookmarklet.images').append('<a href="#"><img src="'+ image_url + '"/></a>');
}
});
})()